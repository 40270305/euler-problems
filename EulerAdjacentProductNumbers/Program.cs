﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace EulerAdjacentProductNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
             //string[] files = Directory.GetFiles(@"H:/myString.txt");
            //creating a string to hold the number
            string myText = "";
            int start = 0, end = 13, length = 13, textLength = 1001;
            long result = 1, max = 0;

            //reading string from a file
            try
            {
                myText = File.ReadAllText(@"D:\myString.txt");
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex);
                throw;
            }


            //for (int i = 0; i < textLength; i++)
            while (end < textLength)
            {
                //method is checking part of our string and return the result
               // Console.WriteLine("max = " + max);
                result = CheckingString(start, length, myText);

                //comparing the result with the maximum number
                if (result > max)
                {
                    max = result;
                    
                }
                start++; end++;
                Console.WriteLine("start = " + start + " end = " + end);
                //Console.ReadKey();
            }
            

            //printing the string
            //Console.WriteLine(myText);
            Console.WriteLine("The answer is = " + max);
            


  
            
            Console.ReadKey();
        }

        public static long CheckingString(int start, int length, string text)
        {
            long  product = 1;
            long[] stringNumbers = new long[14];
            Char[] numbers = new Char[14];
            //numbers = text.ToCharArray(start, 13);
            Console.Write("Number = ");
            //casting numbers from string to an array and counting their product
            for (int i = 0; i < length; i++, start++)
            {
                stringNumbers[i] = (long)Char.GetNumericValue(text, start);
                Console.Write(stringNumbers[i]);
                //if any of the numbers is 0 we can break the loop and move on
                if (product == 0)
                {
                   // break;
                    product *= stringNumbers[i];
                }
                else
                {
                   product *= stringNumbers[i];
                }
            }
            Console.WriteLine("\nProduct = " + product);
            return product;
        }
    }
}
