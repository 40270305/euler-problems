﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerSmallestMultiple
{
    class Program
    {
        static void Main(string[] args)
        {

            uint nNumber = 11;
            uint answer = 1;
            uint multiples = 20;

            //starting infite loop to check all the possibole numbers
            while (true)
            {
                //Console.WriteLine("Number beginning " + nNumber);
                if (nNumber % 2 == 0)
                {
                    //this loop is checking if the number is equally divisible through all numbers in the loop
                    for (uint i = multiples; i >= 1; i--)
                    {
                        if (nNumber % i == 0)
                        {
                            answer++;
                            //Console.WriteLine(" counter " + answer);
                            //we adding the counter as a mean to check if all numbers are divisible
                            if (answer == multiples)
                            {
                                //if the answer is correct we found our number
                                answer = nNumber;
                                Console.WriteLine("answer is counter = " + answer);
                                break;
                            }
                        }
                        //otherwise we check another number
                        else
                        {
                            answer = 0;
                            nNumber++;
                            // Console.WriteLine("number in else " + nNumber);
                            break;
                        }
                    }
                    //if our answer was correct we break infinite lloop
                    if (answer == nNumber)
                    {
                        Console.WriteLine("Breaking loop Number is = " + answer);
                        break;
                    }
                }
                else
                {
                    nNumber++;
                }
            }
            Console.ReadKey();
        }
    }
}
