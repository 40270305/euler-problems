﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler1001Prime
{
    class Program
    {
        static void Main(string[] args)
        {
            uint primes = 2, primeOrder = 0, counter = 0, search = 10001;

            //infinite loop to search until the number is found
            while (primeOrder != search)
            {

                //only 2 is an even prime number, so we omit all others
                if (primes % 2 == 0)
                {
                    if(primes == 2)
                    {
                        primeOrder++;
                    }
                    primes++;
                    continue;
                }
                    //if it's odd we check if it's a prime
                else
                {
                    //loop to check if it divides evenly for any other number
                    for (uint j = 2; j <= primes; j++)
                    {
                        //if it doesn't divide evenly through all numbers below itself it is a prime
                        if (primes % j != 0)
                        {
                            counter++;
                        }
                        else if(primes%j == 0 && primes == j)
                        {
                            break;
                        }
                        else
                        {
                            counter = 0;
                            break;
                        }
                    }
                    //after loop is finished we check if the number is prime
                    if (counter == primes - 2)
                    {
                        //if it is we add our counter and move on to the next search
                        primeOrder++;
                        primes++;
                        counter = 0;
                    }
                    else
                    {
                        primes++;
                    }
                }
                
            }
            primes--;
            Console.WriteLine("Answer is " + primes);
            Console.ReadKey();
        }

        
    }

}
