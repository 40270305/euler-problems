﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummationOfPrimes
{
    class Program
    {
        static void Main(string[] args)
        {
            ulong prime = 3, sum = 0, primeOrder = 1, counter = 10;

           
            while (prime  < 2000001)
            {
                    //checking if number divides through other numbers evenly

                for (ulong j = 3; j <= prime; j += 2)
                {
                    if (prime % j != 0)
                    {
                        continue;
                    }
                    //if it does, but the number is equal to one we're checking it is a prime
                    else if (prime % j == 0 && prime == j)
                    {
                        sum += prime;
                        Console.WriteLine(prime);
                        continue;
                    }
                    else if (prime % j == 0)
                    {
                        break;
                    }

                }
                    prime += 2;
   
            }
            sum += 2;
            prime--;
            Console.WriteLine("The sum of primes below " + counter + " = " + sum);
            Console.ReadKey();

        }

        public static void MyLoopUp(ulong myPrime, ulong mySum)
        {
            ulong prime = myPrime;
            ulong sum = mySum;
            for (ulong j = 3; j <= prime; j += 2)
            {
                if (prime % j != 0)
                {
                    continue;
                }
                //if it does, but the number is equal to one we're checking it is a prime
                else if (prime % j == 0 && prime == j)
                {
                    sum += prime;
                    Console.WriteLine(prime);
                    continue;
                }
                else if (prime % j == 0)
                {
                    break;
                }

            }
        }

        public static void MyLoopDown(ulong myPrime, ulong mySum)
        {
            ulong prime = myPrime;
            ulong sum = mySum;
            for (ulong j = prime - 2; j <= prime; j -= 2)
            {
                if (prime % j != 0)
                {
                    continue;
                }
                //if it does, but the number is equal to one we're checking it is a prime
                else if (prime % j == 0 && prime == j)
                {
                    sum += prime;
                    Console.WriteLine(prime);
                    continue;
                }
                else if (prime % j == 0)
                {
                    break;
                }

            }
        }
        
    }
}
