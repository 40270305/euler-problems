﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerLargestPrimeFactor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Please enter a number to find its largest prime factor: \t");
            ulong nNumber = ulong.Parse(Console.ReadLine());
            ulong answer = 0;

            //
            for (ulong j = 2; j <= nNumber; j++)
            {
                if (nNumber % j == 0)
                {
                    nNumber /= j;
                    j = 2;
                }
               else
               {
                   answer = nNumber;
               }
            }

            Console.WriteLine("Largest prime factor is = " + answer + ".");
            Console.ReadKey();
        }
    }
}
