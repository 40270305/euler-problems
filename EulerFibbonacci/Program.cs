﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* Project Euler Problem 2 - Even Fibbonacci Numbers
 * Author: Karol Pasierb
 * Software Development Year 2
 * 
 * This is the solution to the 2nd problem on Project Euler
 * https://projecteuler.net/archives
 * 
 * Corrected on 27/09/2016
 */

namespace EulerFibbonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            //declaring variable for fibbonbaccis sum
            ulong fibTotal = 0;
            ulong a = 1, b = 1, c = 1;

            while (a < 4000000)
            {
                c = a + b;
                a = b;
                b = c;

                //checking if new number is even and adding it to the sum
                if (c%2 == 0)
                {
                    fibTotal += c;
                }
            }

            Console.WriteLine("the sum of even fibbonaci numbers below 4 million = " + fibTotal + ".");
            Console.ReadKey();
        }
    }
}
