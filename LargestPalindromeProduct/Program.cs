﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LargestPalindromeProduct
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("A palindromic number reads the same both ways. \nThe largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.");
            Console.WriteLine("Find the largest palindrome made from the product of two 3-digit numbers.");
            uint prodA = 999, prodB = 999, result = 0;
            bool match = true;
            string product;

            while (match == true )
           //for (int i = 0; i < 60; i++)
            {
                result = prodA * prodB;
                product = result.ToString();
                //Console.WriteLine("resut = " + result);
                if(product[0] == product[5] && product[1] == product[4] && product[2] == product[3] )
                {
                    match = false;
                }
                else
                {
                    if (prodB == prodA - 100)
                    {
                        prodA--;
                        prodB = prodA;
                    }
                    else
                    {
                        prodB--;
                    }
                    Console.WriteLine("A = " + prodA + " B = " + prodB);
                }  
            }
            Console.WriteLine("Largest palindrome product = " + result);

            Console.ReadKey();

        }
    }
}
