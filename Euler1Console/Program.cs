﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/* Project Euler Problem 1 - Multiples of 3 and 5
 * Author: Karol Pasierb
 * Software Development Year 2
 * 
 * This is the solution to the 1st problem on Project Euler
 * https://projecteuler.net/archives
 * 
 * Corrected on 27/09/2016
 */


namespace Euler1Console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This program counts the sum of natural numbers multiples of 3 and 5 below 1000.");

            int result = 0;

            for (int i = 0; i < 1000; i++)
            {
                if (i % 3 == 0 || i % 5 == 0)
                {
                    result += i;
                }
            }

            Console.WriteLine("The sum of multiples of 3 and 5 below 1000 = " + result);
            Console.ReadKey();
        }
    }
}