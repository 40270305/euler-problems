﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EulerSumSquare
{
    class Program
    {
        static void Main(string[] args)
        {
            uint sum = 0, square = 0, result = 0, i = 1, numbers = 100;
            for (; i <= numbers; i++)
            {
                square += (i * i);
                sum += i;
            }
            sum *= sum;
            result = sum - square;
            Console.WriteLine("the answer is " + sum + " - " + square + " = " + result);
            Console.ReadKey();
        }
    }
}
